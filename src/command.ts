import {
  ChatInputApplicationCommandData,
  Client,
  Interaction,
} from "discord.js";

export type Command = {
  definition: ChatInputApplicationCommandData;
  run: CommandHandler;
};

export type CommandHandler = (interaction: Interaction, client: Client) => void;
