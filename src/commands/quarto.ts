import { Command, CommandHandler } from "../command";
import { createCanvas } from "@napi-rs/canvas";
import {
  ActionRowBuilder,
  BufferResolvable,
  ButtonComponent,
  MessageActionRowComponentBuilder,
} from "discord.js";

type Piece = {
  blue: boolean;
  circ: boolean;
  line: boolean;
  hole: boolean;
};

type Board = (Piece | null)[][];

let board = (() => {
  let output: Board = [[], [], [], []];
  [0, 1, 2, 3].forEach((row) => {
    const blue = row >= 2;
    const hole = row % 2 == 0;
    [0, 1, 2, 3].forEach((col) => {
      const line = col % 2 == 0;
      const circ = col >= 2;

      output[row].push({ blue, circ, line, hole });
    });
  });
  return output;
})();

const drawBoard: () => BufferResolvable = (): BufferResolvable => {
  const color = {
    gray: "#4d4d4d",

    red: "#f55",
    redLight: "#fee",
    blue: "#88f",
    blueLight: "#eef",
  };

  const marginSize = 25;
  const tileSize = 100;
  const canvasSize = 3 * marginSize + 4 * tileSize;
  const paddingSize = 20;
  const holeRadius = { circle: 15, square: 16 } as const;

  const canvas = createCanvas(canvasSize, canvasSize);
  const ctx = canvas.getContext("2d");
  ctx.font = `bold ${tileSize / 4}px monospace`;
  ctx.lineWidth = 6;

  const drawShape = (x: number, y: number, piece: Piece) => {
    ctx.fillStyle = piece.blue ? color.blue : color.red;
    if (piece.circ) {
      ctx.beginPath();
      ctx.arc(
        x + tileSize / 2,
        y + tileSize / 2,
        tileSize / 2 - paddingSize,
        0,
        Math.PI * 2,
      );
      ctx.fill();
      if (piece.line) {
        ctx.strokeStyle = piece.blue ? color.blueLight : color.redLight;
        ctx.beginPath();
        ctx.arc(
          x + tileSize / 2,
          y + tileSize / 2,
          tileSize / 2 - paddingSize,
          0,
          Math.PI * 2,
        );
        ctx.stroke();
      }
    } else {
      ctx.fillRect(
        x + paddingSize,
        y + paddingSize,
        tileSize - 2 * paddingSize,
        tileSize - 2 * paddingSize,
      );

      if (piece.line) {
        ctx.strokeStyle = piece.blue ? color.blueLight : color.redLight;
        ctx.beginPath();
        ctx.rect(
          x + paddingSize,
          y + paddingSize,
          tileSize - 2 * paddingSize,
          tileSize - 2 * paddingSize,
        );
        ctx.stroke();
      }
    }

    if (piece.hole) {
      ctx.fillStyle = color.gray;
      ctx.beginPath();
      ctx.arc(
        x + tileSize / 2,
        y + tileSize / 2,
        piece.circ ? holeRadius.circle : holeRadius.square,
        0,
        Math.PI * 2,
      );
      ctx.fill();
    }
  };

  [0, 1, 2, 3].forEach((row) => {
    const y = row * (marginSize + tileSize);
    [0, 1, 2, 3].forEach((col) => {
      const letter = ["A", "B", "C", "D"][col];
      const x = col * (marginSize + tileSize);
      const piece = board[row][col];

      ctx.fillStyle = color.gray;
      ctx.fillRect(x, y, tileSize, tileSize);

      ctx.fillStyle = "white";
      ctx.fillText(
        letter + row,
        x + tileSize / 2 - 15,
        y + tileSize / 2 + 5,
        tileSize / 2,
      );

      drawShape(x, y, piece!);
    });
  });

  return canvas.toBuffer("image/png");
};

const emptyBoard: Board = [
  [null, null, null, null],
  [null, { blue: true, line: false, circ: true, hole: true }, null, null],
  [null, null, null, null],
  [null, null, null, null],
] as const;

board = emptyBoard;

//let board = emptyBoard;

const handler: CommandHandler = async (interaction, client) => {
  if (!interaction.isChatInputCommand()) return;
  await client.channels.fetch(interaction.channelId);

  const buttons = new ActionRowBuilder<MessageActionRowComponentBuilder>();

  await interaction.reply({
    fetchReply: true,
    files: [drawBoard()],
    //components: [buttons],
  });
};

export const Quarto: Command = {
  definition: {
    name: "quarto",
    description: "[WIP] start a new game of Quarto",
  },
  run: handler,
};
