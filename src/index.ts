import { Client, Events } from "discord.js";
import { QuartobotToken } from "./secret";
import { Quarto } from "./commands/quarto";

const client = new Client({ intents: [] });
console.log("Bot is starting...");

const allCommands = [Quarto];
client.on("ready", async () => {
  if (!client.user || !client.application) return;

  await client.application.commands.set(allCommands.map((c) => c.definition));

  console.log(`${client.user.username} is online`);
});

client.on(Events.InteractionCreate, async (interaction) => {
  if (!interaction.isChatInputCommand()) return;

  const command = allCommands.find(
    (c) => c.definition.name == interaction.commandName,
  );

  if (!command) {
    await interaction.reply(
      "whoopsie, something got silly.\n idk that command :( ",
    );
    return;
  }

  command.run(interaction, client);
});

client.login(QuartobotToken);
